scalaVersion := "2.13.0"

lazy val http4sV = "0.23.0"

libraryDependencies ++= Seq(
"org.http4s" %% "http4s-core" % http4sV,
"org.http4s" %% "http4s-blaze-client" % http4sV
)