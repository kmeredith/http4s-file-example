# Demo

## Download https://raw.githubusercontent.com/http4s/http4s/main/README.md  to http4-readme.md locally

```
sbt:http4s-file-example> run https://raw.githubusercontent.com/http4s/http4s/main/README.md http4-readme.md
...
[info] running net.Main https://raw.githubusercontent.com/http4s/http4s/main/README.md http4-readme.md
...
[success] Total time: 3 s, completed Sep 10, 2021, 4:04:09 PM
```

## Examine the file

```
$head -10 http4-readme.md 
# Http4s [![Build Status](https://github.com/http4s/http4s/workflows/Continuous%20Integration/badge.svg?branch=main)](https://github.com/http4s/http4s/actions?query=branch%3Amain+workflow%3A%22Continuous+Integration%22) [![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.http4s/http4s-core_2.12/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.http4s/http4s-core_2.12) [![Typelevel library](https://img.shields.io/badge/typelevel-library-green.svg)](https://typelevel.org/projects/#http4s) <a href="https://typelevel.org/cats/"><img src="https://typelevel.org/cats/img/cats-badge.svg" height="40px" align="right" alt="Cats friendly" /></a>

Http4s is a minimal, idiomatic Scala interface for HTTP services.  Http4s is
Scala's answer to Ruby's Rack, Python's WSGI, Haskell's WAI, and Java's
Servlets.

```scala
val http = HttpRoutes.of {
  case GET -> Root / "hello" =>
    Ok("Hello, better world.")
$
```