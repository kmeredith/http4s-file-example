package net

import cats.effect.{ExitCode, IO, IOApp}
import fs2.Stream
import fs2.text
import fs2.io.file.Files
import scala.concurrent.ExecutionContext
import org.http4s._
import org.http4s.blaze.client.BlazeClientBuilder

object Main extends IOApp {

  private def writeToFile(bytes: Stream[IO, Byte], filePath: String): Stream[IO, Unit] =
    bytes
      .through(Files[IO].writeAll(java.nio.file.Path.of(filePath)))

  override def run(args: List[String]): IO[ExitCode] = args match {
    case uriArg :: fileName :: Nil =>
      val s: Stream[IO, Unit] =
        for {
          uri <- Stream.fromEither[IO](Uri.fromString(uriArg))
          client = BlazeClientBuilder
            .apply[IO](ExecutionContext.global)
            .resource
          c <- Stream.resource(client)
          req = Request[IO](method = Method.GET, uri = uri)
          resp <- Stream.resource(c.run(req))
          _ <- writeToFile(resp.body, fileName)
        } yield ()

      s.compile.drain.as(ExitCode.Success)
    case notSingle =>
      IO(println(s"Expected a single argument, but received: ${notSingle.size}.")).as(ExitCode.Error)
  }
}